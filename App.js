/**
 * 
 * Open up App
 */
import React, {Component} from 'react';
import {ScrollView, StyleSheet, View} from 'react-native';
import Header from './components/Header';
import AppNav from './components/AppNav';
import EventListing from './components/EventListing';

class App extends Component {
  constructor(props){
    super(props)
    this.state = {
      aboutScreen: false,
      sponsorsScreen: false,
      eventsScreen: true
    }
  }
  
  render() {
      return (
        <View style={styles.container}>
          <Header style={styles.header} headerText={"Events"}/>
          <ScrollView style={styles.body}>
            <Text>Events here</Text>
            <Text>Events here</Text>
            <Text>Events here</Text>
            <Text>Events here</Text>
            <Text>Events here</Text>
            <Text>Events here</Text>
            <Text>Events here</Text>
            <Text>Events here</Text>
            <Text>Events here</Text>
            <Text>Events here</Text>
            <Text>Events here</Text>
            <Text>Events here</Text>
            <Text>Events here</Text>
            <Text>Events here</Text>
            <Text>Events here</Text>
            <Text>Events here</Text>
            <Text>Events here</Text>
            <Text>Events here</Text>
            <Text>Events here</Text>
            <Text>Events here</Text>
            <Text>Events here</Text>
            <Text>Events here</Text>
            <Text>Events here</Text>
            <Text>Events here</Text>
            <Text>Events here</Text>
            <Text>Events here</Text>
            <Text>Events here</Text>
            <Text>Events here</Text>
            <Text>Events here</Text>
            <Text>Events here</Text>
            <Text>Events here</Text>
            <Text>Events here</Text>
            <Text>Events here</Text>
            <Text>Events here</Text>
          </ScrollView>
          <AppNav/>
        </View>
      );
    }
  }

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  body:{
    padding:10
  },
});

export default App;