import React, { Component } from 'react'
import { StyleSheet, Text, View } from 'react-native'

class EventListing extends Component {
    render() {
        return (
            <View style={styles.eventItem}>
                <View style={styles.round}>
                    <Text style={styles.dateMonth}>Jan</Text>
                    <Text style={styles.dateDay}>12</Text>
                </View>

                <View style={styles.eventText}>
                    <Text style={styles.eventItemTitle}>Keeping up with technology in the ever changing environment.</Text>
                </View>
                <View style={styles.arrow}>
                    <Text style={styles.arrowTip}> > </Text>
                </View>
          </View>
        )
    }
}

const styles = StyleSheet.create({
    eventItem: {
        flex: 1,
        flexDirection: 'row',
        paddingTop: 30,
        paddingBottom: 30,
        paddingLeft: 20,
        paddingRight: 20,
        borderBottomWidth: 1,
        borderBottomColor: '#ccc',
    },
    round: {
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 100,
        backgroundColor: '#3682A8',
        width: 100,
        height: 100,
      },
      dateMonth: {
          //textTransform: 'uppercase'
      },
      dateDay: {
          fontSize: 30,
          fontWeight: '900'
      },
      arrow: {
        justifyContent: 'center',
        alignItems: 'center',
      },
      arrowTip: {
        fontSize: 20,
      },
      eventText: {
        flex: 1,
        flexDirection: 'column',
        paddingLeft: 20,
        justifyContent: 'center',
      },
      eventItemTitle: {
          fontSize: 18,
          letterSpacing: 0.2,
          color: '#3682A8',
          fontWeight: 'bold'
      },
      eventTiming: {
        flexDirection: 'row',
        marginBottom: 10
      },
      time: {
        flex: 0.5,
        alignSelf: 'flex-end',
        fontWeight: '100',
        fontSize: 18,
        letterSpacing: 0.2
    },
    date: {
        flex: 1,
        alignSelf: 'flex-start',
        fontWeight: '100',
        fontSize: 18,
        letterSpacing: 0.2    
      }
});
  
export default EventListing;