import React, { Component } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';

class AppNav extends Component {
  render() {
    return (
      <View style={styles.navbar}>
          <View style={styles.icon}>
            <Icon name="calendar" size={30} color="#fff"/>
            <Text style={styles.iconText}>Events</Text>
          </View>
          <View style={styles.icon}>
            <Icon name="heart" size={30} color="#fff" />
            <Text style={styles.iconText}>Sponsors</Text>
          </View>
          <View style={styles.icon} color="#fff" >
            <Icon name="cog" size={30} color="#fff" />
            <Text style={styles.iconText}>About</Text>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  navbar: {
      backgroundColor: '#6295B2',
      padding:10,
      position: 'absolute',
      flex:0.1,
      flexDirection:'row',
      left: 0,
      right: 0,
      bottom: -10,
  },
  icon:{
    flex:1,
    padding:10,
    color:'#fff',
    alignItems:'center',
  },
  iconText:{
    color:'#fff',
    textAlign: 'center',
    fontSize: 18
  }
});
  
export default AppNav;