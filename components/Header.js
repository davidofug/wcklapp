import React, { Component } from 'react'
import { StyleSheet, Text, View } from 'react-native'

class Header extends Component {
    constructor(props){
        super(props)
    }
    render() {
        return (
            <View style={styles.header}>
                <Text style={styles.headerText}>{this.props.headerText}</Text>
            </View> 
        )
    }
}

const styles = StyleSheet.create({
    header: {
      justifyContent: 'center',
      alignItems: 'center',
      padding: 15,
      borderBottomWidth: 1,
      borderBottomColor: '#ccc',
      textTransform: 'uppercase'
    },
    headerText: {
        fontSize: 18,
    }
});
  
export default Header;