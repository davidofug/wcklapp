/**
 * 
 * Open up App
 */
import React, {Component} from 'react';
import {ScrollView, StyleSheet, Text, View} from 'react-native';
import Header from '../components/Header';
import EventListing from '../components/EventListing';
import AppNav from './components/AppNav';

class App extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Header></Header>
        <ScrollView style={styles.scroll}>
          <EventListing />
          <EventListing />
          <EventListing />
          <EventListing />
          <EventListing />     
        </ScrollView>
        <AppNav style={styles.navbar}>
          <Text>Navbar</Text>
        </AppNav>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  scroll: {
    flex: 1,
  },
  navbar: {
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fc0',
  }
});

export default App;